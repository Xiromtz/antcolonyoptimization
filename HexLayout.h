#pragma once
#include "Point.h"
#include <cassert>

class HexLayout
{
public:

	const float size;
	const Point origin;
	const int directions[6][3]{ { 1, 0, -1 },{ 1, -1, 0 },{ 0, -1, 1 },{ -1, 0, 1 },{ -1, 1, 0 },{ 0, 1, -1 } };
	
	enum HEX_DIRECTIONS
	{
		DOWN_RIGHT = 0, UP_RIGHT = 1, UP = 2,
		UP_LEFT = 3, DOWN_LEFT = 4, DOWN = 5
	};

	HexLayout(const float size, const Point& origin);
	~HexLayout();

	const int* getDirection(HEX_DIRECTIONS direction) const;
};