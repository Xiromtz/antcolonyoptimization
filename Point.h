#pragma once

class Point
{
public:
	const double x, y;
	Point(double x, double y) : x(x), y(y) {}
};