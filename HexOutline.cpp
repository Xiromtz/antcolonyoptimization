#include "HexOutline.h"

HexOutline::HexOutline(Hex hex, const sf::Color& color) : hex(hex)
{
    outline.setPointCount(6);
    outline.setOutlineThickness(5.0f);
    outline.setOutlineColor(color);
    outline.setFillColor(sf::Color::Transparent);
}
HexOutline::~HexOutline(){}
void HexOutline::draw(sf::RenderWindow* window)
{
    std::vector<Point> corners = hex.corners();
    for (size_t i = 0; i < 6; i++)
    {
        Point corner = corners[i];
        outline.setPoint(i, sf::Vector2f(corner.x, corner.y));
    }
    window->draw(outline);
}
