
#include <SFML/Graphics.hpp>
#include "PathFinder.h"
#include "Map.h"
#include "HexOutline.h"
#include <algorithm>

bool getMouseOverHex(size_t mouseX, size_t mouseY, const std::unordered_set<Hex>& map, Hex& hex);

int main()
{
	sf::RenderWindow window(sf::VideoMode(1600, 1200), "MapRunner");

	Map* map = new Map(&window, Point(50, 50), 20, 50, 30);
	PathFinder pathFinder(map);

    Hex start;
    bool hasStart = map->at(1, 15, start);
    assert(hasStart);

    HexOutline startOutline(start, sf::Color(0, 102, 0));

    std::vector<std::vector<Hex>> paths;
    std::vector<HexOutline> goals;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

            if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
            {
                Hex mouseOverHex;
                bool foundHex = map->atCoord(event.mouseButton.x, event.mouseButton.y, mouseOverHex);

                if (foundHex)
                {
                    if (mouseOverHex == start || mouseOverHex.getType().cost < 0)
                        continue;

                    HexOutline goalOutline(mouseOverHex, sf::Color::Red);
                    auto it = std::find(goals.begin(), goals.end(), goalOutline);
                    if (it == goals.end())
                    {
                        paths.push_back(pathFinder.findPath(start, mouseOverHex));
                        goals.push_back(goalOutline);
                    }
                    else
                    {
                        auto index = std::distance(goals.begin(), it);
                        paths.erase(paths.begin() + index);
                        goals.erase(goals.begin() + index);
                    }
                }
            }
		}

		window.clear();
		map->draw();
	    startOutline.draw(&window);
        
        for (auto goalOutline : goals)
        {
            goalOutline.draw(&window);
        }
        
        for (auto path : paths)
        {
            pathFinder.drawPath(&window, path);
        }
        
		window.display();
	}

	delete map;

	return 0;
}