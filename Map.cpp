#include "Map.h"

Map::Map(sf::RenderWindow* window, const Point& origin, const float hex_size, size_t width, size_t height) 
	: window(window)
{
	layout = new HexLayout(hex_size, origin);
	shape = new HexShape(window);
    mapLayout = new MapLayout(width, height);

	for (int q = 0; q < mapLayout->width; q++)
	{
		int q_offset = q >> 1;
		for (int r = -q_offset; r < (mapLayout->height - q_offset); r++)
		{
			int realR = r + q_offset;
			int index = (realR * mapLayout->width) + q;
			HexType* type = mapLayout->layout[index];
			map.insert(Hex(q, r, -q - r, layout, shape, type));
		}
	}
}

Map::~Map()
{
	std::cout << "destruction" << std::endl;
    delete shape;
    delete layout;
    delete mapLayout;
}

void Map::draw()
{
	for (auto it = map.begin(); it != map.end(); it++)
	{
		Hex h = *it;
		h.draw(h.getType().color);
	}
}

bool Map::at(const int q, const int r, Hex& hex) const
{
	auto it = map.find(Hex(q, r, layout, shape, mapLayout->layout[0]));
    if (it != map.end())
        hex = *it;

    return it != map.end();
}

bool Map::atCoord(const size_t x, const size_t y, Hex& hex) const
{
    float min_dist = FLT_MAX;
    const Hex* min_hex = nullptr;
    for (auto it = map.begin(); it != map.end(); ++it)
    {
        Point hexCenter = it->hex_to_pixel();
        sf::Vector2f hexToMouse(x - hexCenter.x, y - hexCenter.y);
        float distSquared = hexToMouse.x * hexToMouse.x + hexToMouse.y * hexToMouse.y;
        if (distSquared < min_dist)
        {
            min_dist = distSquared;
            min_hex = &(*it);
            hex = *it;
        }
    }

    return min_hex != nullptr;
}


std::vector<Hex> Map::getNeighbors(const Hex& hex) const
{
	std::vector<Hex> neighbors;

	for (int i = 0; i < 6; i++)
	{
		HexLayout::HEX_DIRECTIONS direction = static_cast<HexLayout::HEX_DIRECTIONS>(i);

        Hex h;
		bool hasNeighbor = getNeighbor(hex, direction, h);
        if (hasNeighbor)
			neighbors.push_back(h);
	}

	return neighbors;
}


bool Map::getNeighbor(const Hex& hex, HexLayout::HEX_DIRECTIONS direction, Hex& dest) const
{
	const int*  directionArray = layout->getDirection(direction);
	Hex h(directionArray[0], directionArray[1], directionArray[2], layout, shape, mapLayout->layout[0]);
	Hex toFind = hex + h;
	return at(toFind.q, toFind.r, dest);
}