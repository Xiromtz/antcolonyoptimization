#include "HexShape.h"

HexShape::HexShape(sf::RenderWindow* window) : window(window)
{
	shape.setPointCount(6);
	shape.setOutlineThickness(1.0f);
	shape.setOutlineColor(sf::Color::Black);
}

HexShape::~HexShape()
{
}

void HexShape::draw(const std::vector<Point>& corners, const sf::Color& color)
{
	for (int i = 0; i < 6; i++)
	{
		Point corner = corners[i];
		shape.setPoint(i, sf::Vector2f(corner.x, corner.y));
	}
	shape.setFillColor(color);
	window->draw(shape);
}