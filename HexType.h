#pragma once

#include<SFML/Graphics/Color.hpp>

class HexType
{
public:
	HexType(const sf::Color& color, const int cost, const char* name) : color(color), cost(cost), name(name) {};
	~HexType() {};

	const sf::Color color;
	const int cost;
	const char* name;
};

