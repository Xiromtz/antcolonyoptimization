﻿#pragma once
#include <unordered_map>
#include "Map.h"

class PathFinder
{
private:
	
	Map* graph;
	std::unordered_map<Hex, Hex> came_from;
	std::unordered_map<Hex, int> cost_so_far;
	std::vector<Hex> m_path;

	void clearContainers();
	void buildCameFrom(const Hex& start, const Hex& end);
	std::vector<Hex> createPath(const Hex& start, const Hex& end);

	Hex findHighestPriority(const std::unordered_map<Hex, int>& frontier) const;
public:
	PathFinder(Map* graph);
	std::vector<Hex> findPath(const Hex& start, const Hex& end);
	void drawCurrentPath(sf::RenderWindow* window);
    void drawPath(sf::RenderWindow* window, const std::vector<Hex>& path);
};
