﻿#pragma once

#define _USE_MATH_DEFINES
#include <vector>
#include "HexLayout.h"
#include "HexShape.h"
#include "HexType.h"

class Hex
{
private:
	Point getCorner(int corner) const;
	HexLayout* layout;
	HexShape* shape;
	HexType* type;

public:
	int q, r, s;
	
	Hex(){}
	Hex(int q, int r, int s, HexLayout* layout, HexShape* shape, HexType* type);
	Hex(int q, int r, HexLayout* layout, HexShape* shape, HexType* type);

	void draw(const sf::Color& color);

	int length(const Hex& hex) const;
	int distance(const Hex& b) const;

	Point hex_to_pixel() const;
	std::vector<Point> corners() const;

	inline HexType getType() const
	{
		return *type;
	}
	
	bool operator==(const Hex& h) const
	{
		return q == h.q && r == h.r && s == h.s;
	}

	bool operator!= (const Hex& h) const
	{
		return !(*this == h);
	}

	Hex operator+=(const Hex& rhs)
	{
		q += rhs.q;
		r += rhs.r;
		s += rhs.s;

		layout = rhs.layout;
		shape = rhs.shape;

		return *this;
	}

	

	friend Hex operator+(Hex lhs, const Hex& rhs)
	{
		lhs += rhs;
		return lhs;
	}

	Hex operator-=(const Hex& rhs)
	{
		q -= rhs.q;
		r -= rhs.r;
		s -= rhs.r;

		layout = rhs.layout;
		shape = rhs.shape;

		return *this;
	}

	friend Hex operator-(Hex lhs, const Hex& rhs)
	{
		lhs += rhs;
		return lhs;
	}
};
