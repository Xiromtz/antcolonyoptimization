#pragma once
#include "HexType.h"
#include <vector>
#include <iostream>

class MapLayout
{
private:

	HexType* water = nullptr;
	HexType* grass = nullptr;
	HexType* mud = nullptr;
	HexType* sand = nullptr;
	HexType* road = nullptr;

    HexType* getRandHex()
    {
        assert(water != nullptr && grass != nullptr && mud != nullptr && sand != nullptr && road != nullptr);
        int random = rand() % 5;
        switch(random)
        {
            case 0: return water;
            case 1: return grass;
            case 2: return mud;
            case 3: return sand;
            case 4: return road;
            default: return nullptr;
        }
    }

public:

	int width = 20, height = 10;
	std::vector<HexType*> layout;

	MapLayout(int width, int height) : width(width), height(height)
	{
		water = new HexType(sf::Color::Blue, -1, "water");
		grass = new HexType(sf::Color::Green, 2, "grass");
		mud = new HexType(sf::Color(82, 91, 20), 6, "mud");
		sand = new HexType(sf::Color::Yellow, 3, "sand");
		road = new HexType(sf::Color(91, 91, 91), 1, "road");
        layout = std::vector<HexType*>(width * height);
        
        for (size_t i = 0u; i < height * width; i++)
        {
            HexType* randHex = getRandHex();
            assert(randHex != nullptr);
            layout[i] = randHex;
        }
	}
	~MapLayout()
	{
		delete water;
		delete grass;
		delete mud;
		delete sand;
		delete road;
	}
};

