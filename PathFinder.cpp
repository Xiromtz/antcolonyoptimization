﻿#include "PathFinder.h"

PathFinder::PathFinder(Map* graph) : graph(graph){}

std::vector<Hex> PathFinder::findPath(const Hex& start, const Hex& end)
{
	clearContainers();
	buildCameFrom(start, end);
	return createPath(start, end);
}

void PathFinder::clearContainers()
{
	std::vector<Hex> empty1;
	std::swap(m_path, empty1);
	came_from.clear();
	cost_so_far.clear();
}

void PathFinder::buildCameFrom(const Hex& start, const Hex& end)
{
	std::unordered_map<Hex, int> frontier;
	frontier[start] = 0;
	came_from[start] = start;
	cost_so_far[start] = 0;

	while (!frontier.empty())
	{
		Hex current = findHighestPriority(frontier);
		frontier.erase(current);

		if (current == end)
			break;

		std::vector<Hex> neighbors = graph->getNeighbors(current);
		for (auto it = neighbors.begin(); it != neighbors.end(); it++)
		{
			Hex neighbor = *it;
			if (neighbor.getType().cost < 0)
				continue;

			int newCost = cost_so_far[current] + neighbor.getType().cost;

			if (!cost_so_far.count(neighbor) || newCost < cost_so_far[neighbor])
			{
				cost_so_far[neighbor] = newCost;
				frontier[neighbor] = newCost + neighbor.distance(end);
				came_from[neighbor] = current;
			}
		}
	}
}

Hex PathFinder::findHighestPriority(const std::unordered_map<Hex, int>& frontier) const
{
	Hex current;
	int min = 0;
	bool firstIter = true;
	for (auto it = frontier.begin(); it != frontier.end(); it++)
	{
		if (firstIter || it->second < min)
		{
			current = it->first;
			min = it->second;
			firstIter = false;
		}
	}

	return current;
}


std::vector<Hex> PathFinder::createPath(const Hex& start, const Hex& end)
{
	Hex current = end;
	m_path.push_back(current);

	while (current != start)
	{
		current = came_from[current];
		m_path.push_back(current);
	}
	std::reverse(m_path.begin(), m_path.end());
	return m_path;
}

void PathFinder::drawCurrentPath(sf::RenderWindow* window)
{
	if (m_path.empty())
		return;

	sf::VertexArray line(sf::LineStrip, m_path.size());
	for (size_t i = 0; i < m_path.size(); i++)
	{
		Point p = m_path[i].hex_to_pixel();
		line[i].position = sf::Vector2f(p.x, p.y);
		line[i].color = sf::Color::White;
	}

	window->draw(line);
}

void PathFinder::drawPath(sf::RenderWindow* window, const std::vector<Hex>& path)
{
    if (path.empty())
        return;

    sf::VertexArray line(sf::LineStrip, path.size());
    for (size_t i = 0; i < path.size(); i++)
    {
        Point p = path[i].hex_to_pixel();
        line[i].position = sf::Vector2f(p.x, p.y);
        line[i].color = sf::Color::White;
    }

    window->draw(line);
}
