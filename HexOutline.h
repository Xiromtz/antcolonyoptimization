#pragma once

#include "Hex.h"

class HexOutline
{
private:
    Hex hex;
    sf::ConvexShape outline;
public:
    explicit HexOutline(Hex hex, const sf::Color& color);
    ~HexOutline();
    void draw(sf::RenderWindow* window);

    bool operator == (const HexOutline &h)
    {
        return hex == h.hex;
    }
};

