#pragma once

#include <unordered_set>
#include "Hex.h"
#include "MapLayout.h"

namespace std
{
	template <> 
	struct hash<Hex>
	{
		size_t operator()(const Hex& h) const
		{
			hash<int> int_hash;
			size_t hq = int_hash(h.q);
			size_t hr = int_hash(h.r);
			return hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2));
		}
	};
}

class Map
{
private:
	sf::RenderWindow* window;
	std::unordered_set<Hex> map;
	HexLayout* layout;
	HexShape* shape;
	MapLayout* mapLayout;

public:
	Map(sf::RenderWindow* window, const Point& origin, const float hex_size, size_t width, size_t height);
	~Map();
	void draw();

	bool at(const int q, const int r, Hex& hex) const;
    bool atCoord(const size_t x, const size_t y, Hex& hex) const;
	std::vector<Hex> getNeighbors(const Hex& hex) const;
	bool getNeighbor(const Hex& hex, HexLayout::HEX_DIRECTIONS direction, Hex& dest) const;

	inline const std::unordered_set<Hex> getMap() const
	{
		return map;
	}
};

