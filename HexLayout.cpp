#include "HexLayout.h"

HexLayout::HexLayout(const float size, const Point& origin) : size(size), origin(origin)
{
}

HexLayout::~HexLayout()
{
}

const int* HexLayout::getDirection(HEX_DIRECTIONS direction) const
{
	int dirNum = static_cast<int>(direction);
	assert(dirNum >= 0 && dirNum <= 6);
	return directions[dirNum];
}