#pragma once

#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include "Point.h"

class HexShape
{
private:
	sf::RenderWindow* window;
	sf::ConvexShape shape;
public:
	HexShape(sf::RenderWindow* window);
	~HexShape();

	void draw(const std::vector<Point>& corners, const sf::Color& color);
};

