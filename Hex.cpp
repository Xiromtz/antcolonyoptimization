﻿#include "Hex.h"

Hex::Hex(int q, int r, int s, HexLayout* layout, HexShape* shape, HexType* type) 
	: q(q), r(r), s(s), layout(layout), shape(shape), type(type)
{
	assert(q + r + s == 0);
}

Hex::Hex(int q, int r, HexLayout* layout, HexShape* shape, HexType* type) 
	: q(q), r(r), s(-q - r), layout(layout), shape(shape), type(type)
{
	assert(q + r + s == 0);
}

void Hex::draw(const sf::Color& color)
{
	shape->draw(corners(), color);
}

int Hex::length(const Hex& hex) const
{
	return int((abs(hex.q) + abs(hex.r) + abs(hex.s)) / 2);
}

int Hex::distance(const Hex& b) const
{
	return length(*this - b);
}

std::vector<Point> Hex::corners() const
{
	std::vector<Point> corners;
	Point center = hex_to_pixel();
	for (int i = 0; i < 6; i++)
	{
		Point corner = getCorner(i);
		corners.push_back(Point(center.x + corner.x, center.y + corner.y));
	}
	return corners;
}

Point Hex::hex_to_pixel() const
{
	double x = (3.0 / 2.0 * q) * layout->size;
	double y = (sqrt(3.0) / 2.0 * q + sqrt(3.0) * r) * layout->size;
	return Point(x + layout->origin.x, y + layout->origin.y);
}

Point Hex::getCorner(int corner) const
{
	double angle = 2.0 * M_PI * corner / 6;
	return Point(layout->size * cos(angle), layout->size * sin(angle));
}